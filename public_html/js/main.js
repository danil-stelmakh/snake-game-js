var SnakeGame = {
};

SnakeGame.Options = {
    foodAmount: 300,
    startTickTime: 40,
}

SnakeGame.Statistic = function () {
    this.score = 0;
    this.range = 0;
    this.turn = 0;
    this.keyPress = 0;
    this.teleport = 0;
    this.viewPort = jQuery('#gameStatistic');
    this.incrScore = function () {
        this.score++;
    };
    this.incrRange = function () {
        this.range++;
    };
    this.incrTurn = function () {
        this.turn++;
    };
    this.incrKeyPress = function () {
        this.keyPress++;
    };
    this.incrTeleport = function () {
        this.teleport++;
    };
    this.show = function () {
        var html = '<ul>';
        html += '<li>Съедено: <strong>' + this.score + '</strong></li>';
        html += '<li>Пробег: ' + this.range + '</li>';
        html += '<li>Поворотов: ' + this.turn + '</li>';
        html += '<li>Нажатий клавиш: ' + this.keyPress + '</li>';
        html += '<li>Телепортаций: ' + this.teleport + '</li>';
        html += '</ul>';
        this.viewPort.html(html);
    };
    this.viewPort.html();
};

SnakeGame.Point = function (x, y) {
    if (x >= 80) {
        x = 80 - x;
    }
    if (y >= 80) {
        y = 80 - y;
    }
    if (x < 0) {
        x = 80 + x;
    }
    if (y < 0) {
        y = 80 + y;
    }
    this.view = null;
    this.getX = function () {
        return x;
    };
    this.getY = function () {
        return y;
    };
    this.draw = function (layer, className) {
        this.view = jQuery('<div />').addClass(className).css({left: x * 10, top: y * 10});
        layer.append(this.view);
    };
    this.addClass = function(className) {
        if (this.view) {
            this.view.addClass(className);
        }
    };
    this.removeClass = function(className) {
        if (this.view) {
            this.view.removeClass(className);
        }
    };
    this.remove = function () {
        if (this.view) {
            this.view.remove();
        }
    };
    this.getHash = function () {
        return this.getX() + ':' + this.getY();
    };
};

SnakeGame.Food = function () {
    this.items = {};
    this.count = 0;
    this.getCount = function () {
        return this.count;
    };
    this.viewPort = jQuery('#gameFoodLayer');
    this.refeel = function (amount) {
        while (this.count < amount) {
            var newItem = new SnakeGame.Point(Math.floor(Math.random() * 80), Math.floor(Math.random() * 80));
            if (!this.items[newItem.getHash()]) {
                newItem.draw(this.viewPort, 'snakeFood');
                this.items[newItem.getHash()] = newItem;
                this.count++;
            }
        }
    };
    this.tryRemoveFood = function (point) {
        var hash = point.getHash();
        if (this.items[hash]) {
            this.items[hash].remove();
            delete this.items[hash];
            this.count--;
            return true;
        }
        return false;
    };
    this.viewPort.html('');
};

SnakeGame.Snake = function (statistic) {
    this.direction = 2; // 1 - left, 2 - up, 3 - right, 4 - down,
    this.newDirection = 2;
    this.section = [
        new SnakeGame.Point(40, 40)
    ];
    this.food = null,
    this.viewPort = jQuery('#gameSnakeLayer');
    this.setFood = function (food) {
        this.food = food;
    };
    this.setDirection = function (direction) {
        if (this.section.length === 1 || this.direction % 2 !== direction % 2) {
            // Можно сменить направление только в права или лево от текущего,
            // но змейка длинной один элемент может двигатся в любую сторону
            this.newDirection = direction;
        }
    };
    this.drawSection = function (index) {
        var className = index === 0 ? 'snakeHead' : 'snakeSection';
        this.section[index].draw(this.viewPort, className);
    };
    this.getHeadPosition = function () {
        return this.section[0];
    };
    this.checkHeadCollision = function () {
        if (this.section.length < 5) {
            // Короткая змейка не моежет укусить себя
            return;
        }
        var hash = this.getHeadPosition().getHash();
        for (var sectionIndex = 1; sectionIndex < this.section.length; sectionIndex++) {
            if (this.section[sectionIndex].getHash() === hash) {
                throw {
                    message: "Game Over",
                    code: 100500
                };
            }
        }
    };
    this.step = function () {
        statistic.incrRange();
        if(this.direction !== this.newDirection) {
            statistic.incrTurn();
            this.direction = this.newDirection;
        }
        var head = this.getHeadPosition();
        var x = head.getX();
        var y = head.getY();
        var directionIncrement = this.direction > 2 ? +1 : -1;
        if (this.direction % 2) {
            x += directionIncrement;
        } else {
            y += directionIncrement;
        }
        var newHead = new SnakeGame.Point(x, y);
        if (newHead.getX() !== x || newHead.getY() !== y) {
            statistic.incrTeleport();
        }
        this.section.unshift(newHead);
        this.drawSection(0);
        head.addClass('snakeSection');
        head.removeClass('snakeHead');
        if(this.food) {
            if(this.food.tryRemoveFood(newHead)) {
                statistic.incrScore();
                if(this.food.getCount() === 0) {
                    throw {
                        message: "You win!",
                        code: 100600
                    };
                }
            } else {
                this.section[this.section.length - 1].remove();
                this.section.pop();
            }
        }
        this.checkHeadCollision();
    };
    this.viewPort.html('');
}

SnakeGame.App = {
    timeout: null,
    snake: null,
    tickTime: null,
    statistic: null,
    tick: function () {
        try {
            this.snake.step();
            this.timeout = setTimeout(SnakeGame.App.tick.bind(this), this.tickTime);
            this.statistic.show();
        } catch (e) {
            this.statistic.show();
            jQuery('html').unbind('keydown');
            alert(e.message);
            this.run();
        }
    },
    keyDown: function (e) {
        var direction = e.keyCode - 36;
        if (direction >= 1 && direction <= 4) {
            this.statistic.incrKeyPress();
            this.snake.setDirection(direction);
            e.preventDefault();
        }
        return true;
    },
    run: function () {
        this.tickTime = SnakeGame.Options.startTickTime;
        this.statistic = new SnakeGame.Statistic();
        this.snake = new SnakeGame.Snake(this.statistic);
        var food = new SnakeGame.Food();
        food.refeel(SnakeGame.Options.foodAmount);
        this.snake.setFood(food);
        jQuery('html').keydown(SnakeGame.App.keyDown.bind(this));
        this.tick();
    }
};

jQuery(function () {
    SnakeGame.App.run();
});
